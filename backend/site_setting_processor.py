from .models import SiteSettings


def site_setting_processor(request):
    site_settings = SiteSettings.objects.first()
    return {
        'site_setting': site_settings
    }
