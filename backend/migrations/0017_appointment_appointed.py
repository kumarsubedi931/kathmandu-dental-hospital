# Generated by Django 4.0.6 on 2022-07-19 11:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0016_alter_blog_published_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='appointed',
            field=models.BooleanField(default=True),
        ),
    ]
