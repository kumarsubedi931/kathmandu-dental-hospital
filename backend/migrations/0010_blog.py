# Generated by Django 4.0.6 on 2022-07-19 04:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0009_healthysmileseveryday_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('slug', models.CharField(max_length=200, unique=True)),
                ('thumbnail', models.ImageField(height_field='height', upload_to='blogs/', width_field='width')),
                ('image', models.ImageField(blank=True, upload_to='blogs/')),
                ('is_active', models.BooleanField(default=False)),
                ('width', models.IntegerField(editable=False)),
                ('height', models.IntegerField(editable=False)),
            ],
            options={
                'db_table': 'blogs',
            },
        ),
    ]
