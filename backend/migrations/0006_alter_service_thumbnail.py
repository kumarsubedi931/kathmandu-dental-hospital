# Generated by Django 4.0.6 on 2022-07-19 02:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0005_service'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='thumbnail',
            field=models.ImageField(blank=True, height_field='300', null=True, upload_to='services/', width_field='300'),
        ),
    ]
