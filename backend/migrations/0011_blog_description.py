# Generated by Django 4.0.6 on 2022-07-19 04:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0010_blog'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='description',
            field=models.TextField(blank=True),
        ),
    ]
