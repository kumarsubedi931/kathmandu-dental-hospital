# Generated by Django 4.0.6 on 2022-07-28 04:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0021_banner_button_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctor',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
    ]
