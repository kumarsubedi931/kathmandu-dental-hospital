# Generated by Django 4.0.6 on 2022-08-01 03:15

from django.db import migrations, models
import django_quill.fields


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0024_sitesettings'),
    ]

    operations = [
        migrations.CreateModel(
            name='WelcomeMessage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40)),
                ('description', django_quill.fields.QuillField()),
                ('image', models.ImageField(help_text='640 * 700 px', upload_to='welcomes/')),
                ('box_text_title', models.CharField(max_length=30)),
                ('box_text_description', models.CharField(max_length=200)),
                ('phone_number', models.CharField(max_length=30, verbose_name='Phone No.')),
            ],
        ),
    ]
