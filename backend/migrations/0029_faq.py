# Generated by Django 4.0.6 on 2022-08-04 08:11

from django.db import migrations, models
import django_quill.fields


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0028_alter_service_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=200)),
                ('answer', django_quill.fields.QuillField()),
                ('is_active', models.BooleanField(default=False)),
            ],
        ),
    ]
