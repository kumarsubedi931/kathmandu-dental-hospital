# Generated by Django 4.0.6 on 2022-07-18 11:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='fb_link',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='instagram_link',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='tiktok_link',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='twitter_link',
            field=models.URLField(blank=True, null=True),
        ),
    ]
