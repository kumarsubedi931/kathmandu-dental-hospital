from django.contrib import admin
from .models import (Doctor, Service, HealthySmilesEveryday, Blog, Appointment, Banner, SiteSettings, WelcomeMessage,
                     Testimonials,
                     FAQ,
                    Contact,
                     CounterBlocks)
from django.utils.html import mark_safe

# Register your models here.


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ('name', 'speciality', 'get_raw_image', 'is_active')


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('title', 'render_thumbnail','render_image')

    fields = ()

    @admin.display(description='Thumbnail')
    def render_thumbnail(self, obj):
        if obj.thumbnail:
            return mark_safe('<img src="{}" width="{}" height="{}" class="img-responsive"'.format(obj.thumbnail.url, 100, 100))

    @admin.display(description='Image')
    def render_image(self, obj):
        if obj.image:
            return mark_safe('<img src="{}" width="{}" height="{}" class="img-responsive"'.format(obj.image.url, 100, 100))

    class Meta:
        model = Service


@admin.register(HealthySmilesEveryday)
class HealthySmilesEverydayAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_raw_icon', 'is_active')


@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_raw_thumbnail', 'is_active')
    exclude = ('slug',)


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'available_service_', 'appointmented_doctor', 'appointment_date', 'appointment_time', 'appointed')


@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ('main_title', 'small_text_title', 'is_button_enable', 'banner_image','is_active')


@admin.register(SiteSettings)
class SiteSettingAdmin(admin.ModelAdmin):
    fieldsets = (
         ('General Information', {'fields': (('name', 'phone_no'), 'logo', 'address_line_1', 'address_line_2', 'email', 'working_hour')}),
         ('Social Media Link', {'fields': ('facebook_link', 'twitter_link', 'instagram_link', 'tiktok_link')}),
         ('Google Maps / Footer', {'fields': ('google_map_link', 'footer_text')})
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False 
    
    class Meta:
        verbose_name = 'Site Settings'


@admin.register(WelcomeMessage)
class WelcomeMessageAdmin(admin.ModelAdmin):
    fieldsets = (
        ('General Info', {'fields': ('title', 'description', 'image')}),
        ('Small Box Info', {'fields': ('box_text_title', 'box_text_description', 'phone_number')})
    )

    def has_delete_permission(self, request, obj=None):
        return False
    
    def has_add_permission(self, request):
        return False


@admin.register(CounterBlocks)
class CounterBlocksAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Testimonials)
class TestimonialsAdmin(admin.ModelAdmin):
    list_display = ('name', 'profession', 'get_raw_thumbnail')


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    list_display = ('question', 'is_active')


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'phone_no')
