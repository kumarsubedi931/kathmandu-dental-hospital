from django.db import models
from django.utils.html import mark_safe
from django.template.defaultfilters import slugify
from django_quill.fields import QuillField
from django.urls import reverse
# Create your models here.

AVAILABLE_SERVICES = (
        (1, 'Complete Whitening Pack'),
        (2, 'Sedation Dentistry'),
        (3, 'Dental Cleaning'),
        (4, 'Cosmetic Dentistry'),
        (5, 'Fissure Sealants'),
        (6, 'Cleaning with Air Flow'),
        (7, 'Aligning the Teeth'),
        (8, 'Aligning the Teeth'),
        (9, 'Child’s First Dental Visit'),
        (10, 'Another')
    )

DOCTORS = (
        (1, 'Dr. John Doe'),
        (2, 'Dr. Martin Ker'),
        (3, 'Dr. Alexander'),
        (4, 'Dr. Eliz Wilson'),
        (5, 'Other')
)

BANNER_BUTTON = (
    (1, 'Read More'),
    (2, 'Book Now')
)


class Doctor(models.Model):
    name = models.CharField(max_length=100)
    speciality = models.CharField(max_length=20)
    fb_link = models.URLField(max_length=200, null=True, blank=True)
    twitter_link = models.URLField(max_length=200, null=True, blank=True)
    instagram_link = models.URLField(max_length=200, null=True, blank=True)
    tiktok_link = models.URLField(max_length=200, null=True, blank=True)
    image = models.ImageField(upload_to='doctors/')
    is_active = models.BooleanField(default=False)

    @property
    def get_raw_image(self):
        if self.image is not None:
            return mark_safe('<img src="{}" class="img-responsive" width="100"/>'.format(self.image.url))

    class Meta:
        db_table = 'doctors'


class Service(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    description = QuillField()
    is_active = models.BooleanField(default=False)
    thumbnail = models.ImageField(upload_to="services/", height_field="height", width_field="width", null=False, blank=False)
    image = models.ImageField(upload_to="services/", null=True, blank=True)
    width=models.IntegerField(editable=False, default=0)
    height= models.IntegerField(editable=False, default=0)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse('service_detail', kwargs={"slug": self.slug})

    @property
    def get_raw_thumbnail(self):
        if self.image is not None:
            return mark_safe('<img src="{}" width="{}" height="{}" class="img-responsive"'.format(self.image.url, 100, 100))

    class Meta:
        db_table = 'services'


class HealthySmilesEveryday(models.Model):
    name = models.CharField(max_length=200)
    icon = models.CharField(max_length=200)
    description = models.TextField()
    is_active = models.BooleanField(default=False)

    @property
    def get_raw_icon(self):
     return mark_safe('<i class="{}"></i>'.format(self.icon))

    class Meta:
        db_table = "healthy_smiles_everyday"


class Blog(models.Model):
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200, unique=True)
    thumbnail = models.ImageField(upload_to="blogs/", width_field="width", height_field="height")
    image = models.ImageField(upload_to="blogs/", blank=True)
    description = QuillField()
    published_at = models.DateTimeField()
    is_active = models.BooleanField(default=False)
    width = models.IntegerField(editable=False)
    height = models.IntegerField(editable=False)

    class Meta:
        db_table = "blogs"

    def get_absolute_url(self):
        return reverse('blog_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    @property
    def get_raw_thumbnail(self):
        return mark_safe('<img src="{}" class="img-responsive" width="{}" height="{}"/>'.format(self.thumbnail.url,100,100))


class Appointment(models.Model):
     name = models.CharField(max_length=200, blank=True)
     phone = models.CharField(max_length=200, blank=True)
     available_service = models.IntegerField(blank=True, choices=AVAILABLE_SERVICES)
     doctor = models.IntegerField(blank=True, choices=DOCTORS)
     appointment_date = models.DateField(blank=True)
     appointment_time = models.TimeField(blank=True)
     appointed = models.BooleanField(default=False)

     def appointmented_doctor(self):
        return DOCTORS[self.doctor - 1][1]
    
     def available_service_(self):
        return AVAILABLE_SERVICES[self.available_service - 1][1]

class Banner(models.Model):
     main_title = models.CharField(max_length=20, verbose_name='Main Title')
     small_text_title = models.CharField(max_length=50, verbose_name='Sub Title')
     is_button_enable = models.BooleanField(default=False)
     button_link = models.URLField(blank=True, null=True)
     button_text = models.IntegerField(null=True, choices=BANNER_BUTTON, blank=True)
     banner_image = models.ImageField(upload_to='banners/', help_text='Image size should be 1650 * 900')
     is_active = models.BooleanField(default=False)

     def __str__(self):
        return self.main_title

class SiteSettings(models.Model):
    logo = models.ImageField(upload_to='logos/', help_text='Please upload image with size 300px * 50px')
    name = models.CharField(max_length=200)
    address_line_1 = models.CharField(max_length=200)
    address_line_2 = models.CharField(max_length=200)
    phone_no = models.CharField(max_length=12)
    email = models.EmailField(blank=True)
    working_hour = models.CharField(max_length=15)
    facebook_link = models.URLField(blank=True)
    twitter_link = models.URLField(blank=True)
    instagram_link = models.URLField(blank=True)
    tiktok_link = models.URLField(blank=True)
    google_map_link = models.TextField(blank=True)
    footer_text = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class WelcomeMessage(models.Model):
    title = models.CharField(max_length=40)
    description = QuillField()
    image = models.ImageField(upload_to='welcomes/', help_text="640 * 700 px")
    box_text_title = models.CharField(max_length=30)
    box_text_description = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=30, verbose_name='Phone No.')

    def __str__(self):
        return self.title


class CounterBlocks(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    counter_title1 = models.CharField(max_length=40, blank=True)
    counter_title1_count = models.IntegerField(default=0, blank=True)
    counter_title2 = models.CharField(max_length=40, blank=True)
    counter_title2_count = models.IntegerField(blank=True)
    counter_title3 = models.CharField(max_length=40, blank=True)
    counter_title3_count = models.IntegerField(blank=True)


class Testimonials(models.Model):
    name = models.CharField(max_length=100)
    profession = models.CharField(max_length=100)
    content = models.TextField()
    photo = models.ImageField(upload_to='testimonials/', help_text='Image size should be 200px * 200xp', null=True, blank=True)
    is_active = models.BooleanField(default=False)

    def get_raw_thumbnail(self):
        return mark_safe('<img src="{}" class="img-responsive" width="{}" height="{}"/>'.format(self.photo.url,100,100))


class FAQ(models.Model):
    question = models.CharField(max_length=200)
    answer = QuillField()
    is_active = models.BooleanField(default=False)


class Contact(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)
    phone_no = models.CharField(max_length=20)
    subject = models.CharField(max_length=200)
    message = models.TextField(blank=True)
    create_at = models.DateField(auto_now_add=True, editable=False)




