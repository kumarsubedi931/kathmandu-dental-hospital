# Kathmandu Dental Hospital 

### Technology 
 * Django 4 
 * MYSQL
 * Python 3.9
 
### Installation Process 
* Create virtual environment 
``pip install virtualenv environment_folder_name``
* Install all requirements packages 
`` pip install -r requirement.txt ``

### Run The Project 
* Open  project  from vs code 
* Prepare migrations ``` python manage.py makemigrations ```
* Run migration files to create database table ``` python manage.py migrate ```
* Run project in browser  ``` python manage.py runservers ```

