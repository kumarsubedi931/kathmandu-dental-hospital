
from django.urls import path
from. import views
urlpatterns = [
    path('', views.index, name='home'),
    path('about', views.about, name='about'),
    path('service', views.service, name='service'),
    path('service-detail/<slug:slug>/', views.service_detail, name='service_detail'),
    path('contact', views.contact ,name='contact'),
    path('appointment', views.appointment, name='appointment'),
    path('blogs', views.blogs, name='blogs'),
    path('blog-detail/<slug:slug>/', views.blog_detail, name='blog_detail'),
    path('not-found', views._404, name='_404'),
    path('blog-detail', views.blog_detail,name='blog-detail')
]
