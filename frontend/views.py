from django.shortcuts import render
from backend.models import (Doctor,
                        Appointment,
                        AVAILABLE_SERVICES,
                        DOCTORS,
                        Banner,
                        WelcomeMessage,
                        Service,
                        CounterBlocks,
                        Blog,
                        SiteSettings,
                        Testimonials,
                        FAQ,
                        HealthySmilesEveryday)
from django.contrib import messages
from django.core.paginator import Paginator
# Create your views here.
# class HomeView(ListView):
#     template_name = 'index.html'
#     model = Doctor
#     context_object_name = 'doctors'


def index(request):
    banners = Banner.objects.filter(is_active=True)
    welcome_message = WelcomeMessage.objects.first()
    healthy_smiles_everyday = HealthySmilesEveryday.objects.filter(is_active=True)
    services = Service.objects.all()
    counter_block = CounterBlocks.objects.first()
    doctors = Doctor.objects.filter(is_active=True)
    blogs = Blog.objects.filter(is_active=True)
    faqs = FAQ.objects.filter(is_active=True)
    context = {
        'doctors': doctors,
        'banners': banners,
        'welcome_message': welcome_message,
        'healthy_smiles_everyday': healthy_smiles_everyday,
        'services': services,
        'counter_block': counter_block,
        'blogs': blogs,
        'faqs': faqs
    }
    return render(request, 'index.html', context)


def about(request):
    welcome_message = WelcomeMessage.objects.first()
    doctors = Doctor.objects.filter(is_active=True)
    testimonials = Testimonials.objects.filter(is_active=True)
    context = {
        'welcome_message': welcome_message,
        'doctors': doctors,
        'testimonials': testimonials
    }
    return render(request, 'about.html', context)


def blogs(request):
    blogs = Blog.objects.filter(is_active=True)
    paginator = Paginator(blogs, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj': page_obj,
        'page_number': int(page_number) if page_number else ''
    }
    return render(request, 'blogs.html', context)


def blog_detail(request, slug):
    blog = Blog.objects.filter(slug=slug).first()
    context = {
        'blog': blog
    }
    return render(request, 'blog-detail.html', context)


def appointment(request):
    context = {
        'available_services': AVAILABLE_SERVICES,
        'doctors': DOCTORS
    }

    if request.POST:
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        available_service = request.POST.get('available_service')
        doctor = request.POST.get('doctor')
        appointment_date = request.POST.get('appointment_date')
        appointment_time = request.POST.get('appointment_time')
        appointment = Appointment(
            name=name,
            phone=phone,
            available_service=available_service,
            doctor=doctor,
            appointment_date=appointment_date,
            appointment_time=appointment_time
        )
        appointment.save()
        messages.success(request, 'Your Appointment has been successfully sent. We will contact you as soon as possible! Thank You')
    return render(request, 'appointment.html', context)


def service(request):
    services = Service.objects.all()
    context = {
        'services': services
    }
    return render(request, 'service.html',  context)


def service_detail(request, slug):
    service = Service.objects.filter(slug=slug).first()
    context = {
        'service': service
    }
    return render(request, 'service-detail.html', context)


def contact(request):
    return render(request, 'contact.html')


def _404(request):
    return render(request,'404.html')

