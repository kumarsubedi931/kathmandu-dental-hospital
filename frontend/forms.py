from django import forms
from backend.models import Doctor

# AVAILABLE_SERVICES = (
#     ("1", "Complete Whitening Pack")
#     ("2", "Sedation Dentistry")
# )
class AppointmentForm(forms.Form):
    doctors = Doctor.objects.all()
    name = forms.CharField(required=True,label='Your Name',widget=forms.TextInput(attrs={'class':''}))
    phone = forms.CharField(required=True,label='Your Phone')
    available_service = forms.IntegerField(required=True)
    doctor = forms.IntegerField(required=True)
    appointment_date = forms.DateField(required=True)
    appointment_time = forms.TimeField(required=True)
